import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runner.notification.RunListener;
import org.junit.runners.JUnit4;
import roman.RomanConverter;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class Tests {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static RomanConverter romanConverter = new RomanConverter();

    @Test
    public void shouldConvertTORoman()
    {
        assertEquals("V",romanConverter.toRoman(5));
        assertEquals("I",romanConverter.toRoman(1));
    }

    @Test
    public void shouldConvertFromRoman()
    {
        assertEquals(10,romanConverter.fromRoman("X"));
        assertEquals(4,romanConverter.fromRoman("IV"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentwhenConvertFromRoman()
    {
        romanConverter.fromRoman("iii");
        expectedException.expect(IllegalArgumentException.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentwhenConvertToRoman()
    {
        romanConverter.toRoman(-1);
        expectedException.expect(IllegalArgumentException.class);
    }
    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentwhenConvertToRoman2()
    {
        romanConverter.toRoman(5000);
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("number out of range (must be 1..3999)");
    }

}
